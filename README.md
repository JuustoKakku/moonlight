# Moonlight Logger
_An Elite: Dangerous companion app_

Moonlight keeps track of your travels through the E:D galaxy, showing the system details with data fetched from EDSM.net. It also updates your EDSM.net profile automatically when running.

System and body scan data is also submitted to EDDN.

![main](images/mainview.png)

## Usage

Open Moonlight before starting the game and keep it open while playing. Moonlight will keep reading the game journal log, updating the view with your path, and uploading the details to EDSM.net if the login details are set.

## Latest version
Latest version can be downloaded from the bitbucket repository from the **downloads** section.

## Developing

Based on [Modern and Minimal Electron + React Starter Kit](https://github.com/pbarbiero/basic-electron-react-boilerplate).

Runs on top of electron with the UI done with React and MobX.

#### Development
* Run `yarn install`
* Run `yarn start` to start webpack-dev-server. Electron will launch automatically after compilation.

#### Building & packaging
* Run `yarn run package` to have webpack compile your application into `dist/bundle.js` and `dist/index.html`, and then an electron-packager run will be triggered for the current platform/arch, outputting to `builds/`