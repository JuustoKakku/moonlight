### 1.3.4 - 2018-02-27
- Fix missing properties on EDDN journal send

### 1.3.3 - 2018-02-27
- Fix issues with 3.0

### 1.3.2 - 2018-02-27
- Updates for E:D 3.0 for EDSM

### 1.3.1 - 2018-01-27
- Check for Beta version of ED

### 1.3.0 - 2018-01-27
- Change EDSM data upload to use the new Journal API
    - This means more data showing up in your EDSM profile, e.g. fuel, cargo, materials etc.
- Update libraries

### 1.2.0 - 2017-11-5
- Add support for system bookmarking
- Add support for EDSM private comments as notes

### 1.1.2 - 2017-10-29
- Fix system view getting stuck showing Scanning/loading

### 1.1.1 - 2017-10-29
- Fix system view showing bodies from a wrong system

### 1.1.0 - 2017-10-29
-  Upload exploration scan data to EDDN

### 1.0.1 - 2017-10-25
- Improve startup time
- Add monitoring for exploration data selling to update credits

### 1.0.0 - 2017-10-22
- Initial release