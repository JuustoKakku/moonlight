const path = require('path');
const fs = require('fs');
const readline = require('readline');

class JournalMonitor {

  constructor() {
    this.journalPath = path.resolve(process.env.USERPROFILE, 'Saved Games', 'Frontier Developments', 'Elite Dangerous');
    this.lastEvent = '2015-01-01T00:00:00Z';
    this.lastMod = 0;
    this.interval = null;
    this.isLive = true;
  }

  doMonitor(cb) {
    if (!fs.existsSync(this.journalPath)) {
      console.error('Journal folder does not exist!');
      return false;
    }

    if (!cb) {
      console.error('No callback given!');
      return false;
    }

    if (typeof cb !== 'function') {
      console.error('Callback must be a function');
      return false;
    }


    this.fillPreviousSession(cb);

    if (!this.isLive) {
      console.error('Beta version usage not supported!');
      return false;
    }

    if (this.interval !== null) {
      clearInterval(this.interval);
      this.interval = null;
    }

    // console.log('watching', this.journalPath);
    this.interval = setInterval(() => {
      // console.log('tick');
      /*
                Not really the best solution to scan the file every 15s.
                fs.watch doesn't seem to fire when Elite writes on the log.
                Fires fine when editing the file manually though.
            */
      const latest = this.getLatestJournalFile();
      this.readFile(latest, false, cb);
    }, 1000 * 15);
    /*
        watch(this.journalPath, (event, filename) => {
            console.log('Received file event', event, filename);
            if(event === 'update') {
                this.readFile(filename, false, cb);
            }
        });
        */
    return true;
  }

  fillPreviousSession(cb) {
    const latest = this.getLatestJournalFile();
    this.readFile(latest, true, entry => cb(entry, true), entry => entry.event === 'LoadGame' || entry.event === 'Loadout' /*|| entry.event === 'FSDJump'*/ );
  }

  getLatestJournalFile() {
    const files = fs.readdirSync(this.journalPath).filter(file => file.match(/Journal\.\d*\.\d+\.log/i));
    return getMax(files, file => fs.statSync(path.resolve(this.journalPath, file)).mtimeMs);
  }

  /**
     * Reads log entries from a given file.
     * @param {string} filename Log file to read
     * @param {boolean} all Should we read all events, or just new ones after previous update
     * @param {function} cb Callback function to call with each log entry
     */
  readFile(filename, all, cb, filter) {
    // console.log(filename, all);
    const fullPath = path.resolve(this.journalPath, filename);
    const mtimeMs = fs.statSync(fullPath).mtimeMs;
    if (this.lastMod && this.lastMod >= mtimeMs) {
      return;
    }
    this.lastMod = mtimeMs;
    const reader = readline.createInterface({
      input: fs.createReadStream(fullPath)
    });


    if (!filter || typeof filter !== 'function') {
      filter = () => true;
    }

    let lastRead = null;
        

    // Can have this outside the line read, as changes inside one file read
    // won't have effect on that update
    const lastUpdate = new Date(this.lastEvent).getTime() + 1; // 1ms to avoid duplicates
    // console.log('===============================================');
    // console.log('===================NEW READ====================');
    // console.log('===============================================');
    reader.on('line', (line) => {
      if (line) {
        try {
          const entry = JSON.parse(line);
          // console.log(entry.event, '\t\t', entry.timestamp);
          if (!all) {
            if (new Date(entry.timestamp).getTime() < lastUpdate) {
              // console.log('\tskipping, last event is', this.lastEvent);
              return;
            }
          }
          if ((entry.event || '').toLowerCase() === 'fileheader') {
            this.checkVersion(entry);
          }
          if (!this.isLive) {
            console.log('Not live, aborting');
            if(this.interval !== null) {
              clearInterval(this.interval);
              this.interval = null;
            }
            return;
          }
          if(filter(entry)) {
            cb(entry);
          }
          this.lastEvent = entry.timestamp;
          // console.log('new entry', entry.event, this.lastEvent);
        } catch (e) {
          console.error('Log line parse failed!', e);
        }
      }
      if (lastRead !== null) {
        clearTimeout(lastRead);
      }
      lastRead = setTimeout(() => {
        reader.close();
      }, 2000);
    });
  }

  checkVersion(entry) {
    const versionStr = (entry.gameversion || '').toLowerCase();
    if(versionStr.indexOf('beta') >= 0) {
      this.isLive = false;
    }
  }
}

module.exports = new JournalMonitor();

function getMax(array, iteratee) {
  let result = null;
  if(array === null) {
    return result;
  }
  let max;
  for (const value of array) {
    const current = iteratee(value);
    if (current != null && (max === undefined || current > max)) {
      max = current;
      result = value;
    }
  }

  return result;
}
