import axios from 'axios';
import logStore from 'store/log';
import systemStore from 'store/system';
import settingsStore, { settingsHydrate } from 'store/settings';
import { MOONLIGHT_NAME, MOONLIGHT_VERSION } from 'modules/globals';
import { checkForError } from 'store/errors';

const eddnJournalEvents = ['Docked', 'FSDJump', 'Scan', 'Location'];

class JournalPoster {
  constructor() {
    this.eddnClient = axios.create({
      baseURL: 'https://eddn.edcd.io:4430'
    });

    this.edsmClient = axios.create({
      baseURL: 'https://www.edsm.net'
    });

    this.currentStarSystem = null;
    this.currentStarPos = null;
    this.currentSystemAddress = null;
    // this.lastMarketId = null;

    this.refresh = -1;

    this.edsmSkipList = [];
  }


  cleanEntry(entry) {
    const clean = { ...entry };
    delete clean['CockpitBreach'];
    delete clean['BoostUsed'];
    delete clean['FuelLevel'];
    delete clean['FuelUsed'];
    delete clean['JumpDist'];
    delete clean['Latitude'];
    delete clean['Longitude'];

    for (const key in clean) {
      if (key.match(/_Localised$/i)) {
        delete clean[key];
      }
    }

    return clean;
  }

  getCurrentSystem(entry) {
    if (entry.StarSystem) {
      this.currentStarSystem = entry.StarSystem;
    }

    if (entry.StarPos) {
      this.currentStarPos = entry.StarPos;
    }

    if (entry.SystemAddress) {
      this.currentSystemAddress = entry.SystemAddress;
    }

    /*
    if (entry.MarketID) {
      this.lastMarketId = entry.MarketID;
    }
    */
  }

  postEddnLog(entry) {
    if (eddnJournalEvents.indexOf(entry.event) < 0) {
      return;
    }

    // This will be gone when moonlight is closed,
    // hopefully it also prevents data from wrong systems.
    if (!this.currentStarSystem || !this.currentStarPos) {
      return;
    }

    const clean = this.cleanEntry(entry);

    const payload = {
      $schemaRef: 'https://eddn.edcd.io/schemas/journal/1',
      header: {
        uploaderID: logStore.cmdr,
        softwareName: MOONLIGHT_NAME,
        softwareVersion: MOONLIGHT_VERSION
      },
      message: {
        StarSystem: this.currentStarSystem,
        StarPos: this.currentStarPos,
        ...clean
      }
    };
    this.eddnClient.post('upload/', payload)
      .then(() => {
        if (entry.event === 'Scan') {
          // Fetch the system data again so we'll have the latest scans included
          if (this.refresh >= 0) {
            clearTimeout(this.refresh);
          }
          const sys = this.currentStarSystem;
          this.refresh = setTimeout(() => {
            systemStore.fetchSystemData(sys, true);
          }, 30 * 1000);
        }
      })
      .catch(err => console.error(err));
  }

  postEdsmLog(entry) {
    if (!settingsStore.isEdsmLoggedIn) {
      return;
    }

    if (this.edsmSkipList.indexOf(entry.event) >= 0) {
      return;
    }

    if ((!this.currentStarSystem || !this.currentStarPos) && !this.isStartupEvent(entry.event)) {
      return;
    }

    /* EDSM seems to have some issues handling JSON payloads, leave this in case it ever changes 
        const payload = {
            commanderName: settingsStore.edsmUser,
            apiKey: settingsStore.edsmKey,
            fromSoftware: MOONLIGHT_NAME,
            fromSoftwareVersion: MOONLIGHT_VERSION,
            message: {
                ...entry,
                _systemName: this.currentStarSystem,
                _systemCoordinates: this.currentStarPos,
                _shipId: logStore.shipID,
                _stationName: logStore.currentStation
            }
        };
        */

    const msg = {
      ...entry,
      _systemAddress: this.currentSystemAddress,
      _systemName: this.currentStarSystem,
      _systemCoordinates: this.currentStarPos,
      _shipId: logStore.shipID,
      _stationName: logStore.currentStation
      // _marketId: this.lastMarketId // Is this really needed? It's included in the relevant events
    };

    const formPayload = new FormData();
    formPayload.append('commanderName', settingsStore.edsmUser);
    formPayload.append('apiKey', settingsStore.edsmKey);
    formPayload.append('fromSoftware', MOONLIGHT_NAME);
    formPayload.append('fromSoftwareVersion', MOONLIGHT_VERSION);
    formPayload.append('message', JSON.stringify(msg));

    this.edsmClient.post('api-journal-v1', formPayload)
      .then((resp) => {
        checkForError(resp);
      })
      .catch((err) => {
        console.error(err);
      });
  }

  checkEdsmSkipList() {
    this.edsmClient.get('api-journal-v1/discard')
      .then((resp) => {
        if (Array.isArray(resp.data)) {
          this.edsmSkipList = resp.data;
        }
      })
      .catch((err) => {
        console.error(err);
      });
  }

  isStartupEvent(event) {
    return ['Cargo', 'Loadout', 'Materials', 'LoadGame', 'Rank', 'Progress']
      .includes(event);
  }

  postLogs(entry) {
    // console.log('posting', entry.StarSystem, entry.event, entry);
    this.getCurrentSystem(entry);
    // console.log('current system is', this.currentStarSystem, this.currentStarPos);
    this.postEddnLog(entry);
    this.postEdsmLog(entry);
  }

}

const journal = new JournalPoster();

settingsHydrate.then(() => journal.checkEdsmSkipList());

export default journal;
