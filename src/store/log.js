import { observable, action, computed } from 'mobx';
import { create, persist } from 'mobx-persist';
import localForage from 'localforage';
import _ from 'lodash';
import systemStore from 'store/system';
import settingsStore, { edsm } from 'store/settings';
import creditsStore from 'store/credits';
import journal from 'api/journal';
import moment from 'moment';
import { remote } from 'electron';

const monitor = remote.require('./src/modules/journal-monitor');


// const ECO_NONE = '$economy_None;';
// const GOV_NONE = '$government_None;';
// const SEC_NONE = '$GAlAXY_MAP_INFO_state_none;'; // ??? no idea, just a guess

// companion api
// /profile
// /market
// /shipyard


class LogEntry {
    @persist @observable StarSystem = '';
    @persist('list') @observable StarPos = [0, 0, 0];
    @persist @observable Body = '';
    @persist @observable JumpDist = 0;
    @persist @observable FuelUsed = 0;
    @persist @observable FuelLevel = 0;
    @persist @observable BoostUsed = false;
    @persist @observable Faction = '';
    @persist @observable FactionState = ''
    @persist @observable SystemAllegiance = '';
    @persist @observable SystemEconomy = '';
    @persist @observable SystemGovernment = '';
    @persist @observable SystemSecurity = '';
    @persist @observable timestamp = '';
    @persist @observable event = '';
    @persist @observable SystemEconomy_Localised = '';
    @persist @observable SystemGovernment_Localised = '';
    @persist @observable SystemSecurity_Localised = '';
}

class LogStore {
    @persist('list', LogEntry)
    @observable log = [];

    @persist('list', LogEntry)
    @observable bookmarks = [];

    @persist
    @observable cmdr = '';

    @persist
    @observable shipType = '';

    @persist
    @observable shipName = '';

    @persist
    @observable shipID = -1;

    @persist
    @observable shipIdent = '';

    @persist
    @observable credits = 0;

    @persist
    @observable fuelLevel = 0;

    @persist
    @observable fuelCapacity = 0;

    @observable selectedEntry = 0;
    @observable selectedBookmark = 0;

    @observable currentStation = '';

    @action
    selectEntry = (i) => {
      this.selectedEntry = i;
      if (this.log[i]) {
        const systemName = this.log[i].StarSystem;
        systemStore.selectSystem(systemName);
        systemStore.fetchSystemData(systemName);
      }
    }

    @action
    selectBookmark = (i) => {
      this.selectedBookmark = i;
      if (this.bookmarks.length > i && this.bookmarks[i]) {
        const systemName = this.bookmarks[i].StarSystem;
        systemStore.selectSystem(systemName);
        systemStore.fetchSystemData(systemName);
      }
    }

    @action
    addBookmark = (sysName) => {
      const exists = this.bookmarks.filter(item => item.StarSystem === sysName);
      if (exists && exists.length > 0) {
        this.bookmarks = this.bookmarks.filter(item => item.StarSystem !== sysName);
        if(this.selectedBookmark >= this.bookmarks.length) {
          this.selectBookmark(this.bookmarks.length - 1);
        } else {
          this.selectBookmark(this.selectedBookmark);
        }
      } else {
        const sys = this.log.filter(item => item.StarSystem === sysName);
        if(sys.length > 0) {
          this.bookmarks.push(sys[0]);
        }
      }
    }

    @action
    processEntry(entry, skipEdsm) {
      // console.log('parsing', entry.event);

      const exists = _.find(this.log, (item) => {
        return item.timestamp === entry.timestamp;
      });

      if (exists) {
        return;
      }

      switch (entry.event) {
        case 'FSDJump':
          // console.log('jump', entry);
          this.log.unshift(entry);
          if (this.selectedEntry === 0) {
            this.selectEntry(0);
          } else {
            this.selectEntry(this.selectedEntry + 1);
          }

          if (this.log.length > 500) {
            this.log.pop();
          }

          break;
        case 'LoadGame':
          this.cmdr = entry.Commander;
          if (!settingsStore.edsmUser) {
            settingsStore.edsmUser = entry.Commander;
          }
          break;
        case 'FuelScoop':
          this.fuelLevel = entry.Total;
          break;
        case 'RefuelAll':
          this.fuelLevel = this.fuelCapacity;
          break;
        case 'Loadout':
        case 'SetUserShipName':
          this.shipName = entry.UserShipName || entry.ShipName || '';
          this.shipIdent = entry.UserShipId || entry.ShipIdent || '';
          this.shipType = entry.ShipType || entry.Ship;
          this.shipID = entry.ShipID;
          break;
        case 'ModuleBuy':
          this.credits -= entry.BuyPrice;
          if (entry.SellPrice) {
            this.credits += entry.SellPrice;
          }
          creditsStore.pushCreditEvent(this.credits, entry.timestamp);
          break;
        case 'ModuleSell':
        case 'ModuleSellRemote':
          this.credits += entry.SellPrice;
          creditsStore.pushCreditEvent(this.credits, entry.timestamp);
          break;
        case 'PayFines':
        case 'PayLegacyFines':
          this.credits -= entry.Amount;
          creditsStore.pushCreditEvent(this.credits, entry.timestamp);
          break;
        case 'RedeemVoucher':
          this.credits += entry.Amount;
          creditsStore.pushCreditEvent(this.credits, entry.timestamp);
          break;
        case 'ShipyardBuy':
          this.credits -= entry.ShipPrice;
          if (entry.SellPrice) {
            this.credits += entry.SellPrice;
          }
          creditsStore.pushCreditEvent(this.credits, entry.timestamp);
          break;
        case 'ShipyardSell':
          this.credits += entry.ShipPrice;
          creditsStore.pushCreditEvent(this.credits, entry.timestamp);
          break;
        case 'ShipyardTransfer':
          this.credits -= entry.TransferPrice;
          creditsStore.pushCreditEvent(this.credits, entry.timestamp);
          break;
        case 'Promotion':
        case 'Rank':
          break;
        case 'ShipyardSwap':
          break;
        case 'SellExplorationData':
          this.credits += entry.BaseValue;
          this.credits += entry.Bonus;
          creditsStore.pushCreditEvent(this.credits, entry.timestamp);
          break;
        case 'Docked':
          this.currentStation = entry.StationName;
          break;
        case 'Undocked':
          this.currentStation = '';
          break;
        default:
          // do nothing
          break;
      }

      if (entry.Ship) {
        this.shipType = entry.Ship;
      }
      if (entry.ShipID) {
        this.shipID = entry.ShipID;
      }
      if (entry.ShipIdent) {
        this.shipIdent = entry.ShipIdent;
      }
      if (entry.ShipName) {
        this.shipName = entry.ShipName;
      }
      if (entry.FuelLevel) {
        this.fuelLevel = entry.FuelLevel;
      }
      if (entry.FuelCapacity) {
        this.fuelCapacity = entry.FuelCapacity;
      }
      if (entry.Reward) {
        this.credits += entry.Reward;
      }
      if (entry.TotalSale) {
        this.credits += entry.TotalSale;
      }
      if (entry.Cost) {
        this.credits -= entry.Cost;
      }
      if (entry.TotalCost) {
        this.credits -= entry.TotalCost;
      }
      if (this.fuelCapacity < this.fuelLevel) {
        // Fuel capacity is only reported on loadgame, but not when switching ships
        this.fuelCapacity = this.fuelLevel;
      }
      if (entry.Credits) {
        this.credits = entry.Credits;
        creditsStore.pushCreditEvent(entry.Credits, entry.timestamp, true);
      }

      if (!skipEdsm) {
        journal.postLogs(entry);
      }
    }

    @computed
    get jumpLog() {
      return this.log
        .filter(entry => entry.event === 'FSDJump');
    }

    cleanUp = () => {
      if (this.log.length === 0) {
        return;
      }
      const first = moment(this.log[this.log.length - 1].timestamp);
      this.log = this.log.filter(e => moment(e.timestamp).isSameOrAfter(first));
    }

    isBookmarked = (sysName) => {
      return computed(() => this.bookmarks.filter(item => item.StarSystem === sysName).length > 0).get();
    }

}

const hydrate = create({
  storage: localForage,
  jsonify: false
});

const store = new LogStore();

const logHydrate = hydrate('log', store);
logHydrate
  .then(() => {
    // console.log('system log hydrated');
    // Wrap the mobx action in another local function to ensure the store updates.
    // If action is passed directly to the remote, mobx can't react to the changes and
    // trigger observers.
    if (!monitor.doMonitor((entry, skipEdsm) => store.processEntry(entry, skipEdsm))) {
      console.error('Failed to start file monitor!');
    }

    store.selectEntry(0);
    // store.cleanUp();
  });

export default store;
export { logHydrate, edsm };
