import { observable, action, computed } from 'mobx';
import axios from 'axios';
import _ from 'lodash';
import settingsStore from 'store/settings';
import { checkForError } from 'store/errors';

const edsmSystem = axios.create({
  baseURL: 'https://www.edsm.net/api-system-v1/'
});

const edsmLog = axios.create({
  baseURL: 'https://www.edsm.net/api-logs-v1/'
});

class SystemStore {

    @observable
    systems = new Map();

    @observable
    notes = new Map();

    @observable
    selectedSystemName = '';

    @observable
    systemsLoading = new Set();

    @computed
    get selectedSystem() {
      if (this.systems.has(this.selectedSystemName)) {
        return this.systems.get(this.selectedSystemName);
      }
      return null;
    }

    @action
    selectSystem(systemName) {
      this.selectedSystemName = systemName;
    }

    @action
    fetchSystemData(systemName, force) {
      if (this.systems.has(systemName) && !force) {
        return Promise.resolve(this.systems.get(systemName));
      }
      this.systemsLoading.add(systemName);

      const bodies = edsmSystem.get('bodies', {
        params: {
          systemName
        }
      });

      const stations = edsmSystem.get('stations', {
        params: {
          systemName
        }
      });

      this.loadSystemComment(systemName);

      Promise.all([bodies, stations])
        .then(action('system-data-success', ([b, s]) => {
          // EDSM likes to randomly return an empty array instead of a proper response,
          // so need to check the bodies array is there
          const both = b.data && b.data.bodies ? [...b.data.bodies] : [];

          const stationsArr = s.data && s.data.stations ? s.data.stations : [];
          stationsArr.forEach(station => {
            const targetBody = _.minBy(both, body => {
              if (body.type !== 'Planet' && body.type !== 'Star') {
                return Number.MAX_VALUE;
              }
              return Math.abs(station.distanceToArrival - body.distanceToArrival);
            });

            // Set offset to zero if we have no bodies, only stations
            station.offset = targetBody && b.data.bodies.length > 0 ? targetBody.offset + 1 : 0;
            both.splice(both.indexOf(targetBody) + 1, 0, station);

          });

          this.systems.set(systemName, { ...b.data, bodies: both });
          this.systemsLoading.delete(systemName);
        }))
        .catch(action('system-data-failure', (error) => {
          console.error('Failed to fetch system data', error);
          this.systemsLoading.delete(systemName);
        }));
    }

    @action
    loadSystemComment(systemName) {
      if (!systemName || this.notes.has(systemName) || !settingsStore.enableComments || !settingsStore.isEdsmLoggedIn) {
        return;
      }
      const params = {
        commanderName: settingsStore.edsmUser,
        apiKey: settingsStore.edsmKey,
        systemName
      };

      edsmLog.get('get-comment', { params })
        .then(action('system-comment-success', response => {
          checkForError(response);
          //console.log(response);
          const { data } = response;
          if (data.msgnum === 100) {
            this.notes.set(systemName, data.comment);
          } else if (data.msgnum === 101) {
            this.notes.set(systemName, '');
          }
          //console.log(this.notes);
        }))
        .catch(err => console.error(err));
    }

    @action
    updateSystemComment(systemName) {
      if (!systemName || !this.notes.has(systemName) || !settingsStore.isEdsmLoggedIn) {
        return;
      }
      const data = new URLSearchParams();

      data.append('commanderName', settingsStore.edsmUser);
      data.append('apiKey', settingsStore.edsmKey);
      data.append('systemName', systemName);
      data.append('comment', this.notes.get(systemName));

      edsmLog.post('set-comment', data)
        .then(response => checkForError(response))
        .catch(err => console.error(err));
    }

    @action
    setComment(systemName, comment) {
      this.notes.set(systemName, comment);
    }

    @computed
    get currentComment() {
      return this.notes.get(this.selectedSystemName) || '';
    }
    isSystemLoading(systemName) {
      return computed(() => this.systemsLoading.has(systemName)).get();
    }

}


const store = new SystemStore();

export default store;
