import { observable, action, computed } from 'mobx';


class ErrorStore {

    @observable
    error = null;

    @computed
    get hasError() {
      return !!this.error;
    }

    @computed
    get latestError() {
      return this.error;
    }

    @action
    pushError(msg) {
      if(typeof msg === 'string') {
        this.error = msg;
      }
    }

    @action
    removeLatestError() {
      this.error = null;
    }

}


const store = new ErrorStore();


const checkForError = (response) => {
  const r = response.data.msgnum;
  // The first two shouldn't be visible, as the 
  // request shouldn't even be made without them
  if (r === 201) {
    store.pushError('Missing EDSM Commander name, check settings!');
  } else if (r === 202) {
    store.pushError('Missing EDSM API key, check settings!');
  } else if (r === 203) {
    store.pushError('Invalid EDSM Commander name or API key, check settings!');
  }
};

export default store;
export { checkForError };
