import { observable } from 'mobx';
import { create, persist } from 'mobx-persist';
import localForage from 'localforage';

const TAB_EXPLORATION = 'exploration';
const TAB_CREDITS = 'credits';
const TAB_CMDR = 'cmdr';
const TAB_FLEET = 'fleet';

const tabs = {
  TAB_CMDR,
  TAB_CREDITS,
  TAB_EXPLORATION,
  TAB_FLEET
};


class NavStore {

    @observable
    @persist
    currentTab = TAB_EXPLORATION;

}



const hydrate = create({
  storage: localForage,
  jsonify: false
});

const store = new NavStore();

const navHydrate = hydrate('nav', store);

window.nav = store;

export default store;
export { tabs, navHydrate };
