import { observable, action, computed } from 'mobx';
import { create, persist } from 'mobx-persist';
import localForage from 'localforage';
import moment from 'moment';
import _ from 'lodash';

class CreditEvent {
    @persist
    @observable
    timestamp = '';

    @persist
    @observable
    credits = 0;
}


class CreditsStore {
    @persist('list', CreditEvent)
    @observable
    history = [];

    @action
    pushCreditEvent(credits, timestamp, force) {
      const latestEvent = this.history.length > 0 ? this.history[this.history.length - 1] : null;

      if (latestEvent && moment(latestEvent.timestamp).add(30, 'minutes').isSameOrAfter(timestamp) && !force) {
        // Only push credit history twice per hour,
        // no need to spam it when buying loads of modules for example
        return;
      }

      const eventExists = this.history.length > 0 && _.find(this.history, event => event.timestamp === timestamp);

      if (!eventExists) {
        this.history.push({
          credits,
          timestamp
        });
      }
    }

    @computed
    get maxCredits() {
      if (this.history.length === 0) {
        return 1;
      }
      const e = _.maxBy(this.history, entry => entry.credits);
      return e.credits;
    }

    @computed
    get firstDate() {
      if (this.history.length === 0) {
        return moment(0).unix();
      }
      return moment(this.history[0].timestamp).unix();
    }

    @computed
    get lastDate() {
      if (this.history.length === 0) {
        return moment().unix();
      }
      return moment(this.history[this.history.length - 1].timestamp).unix();
    }

    cleanUp = () => {
      if(this.history.length > 0) {
        const first = moment(this.history[0].timestamp);
        this.history = this.history.filter(e => moment(e.timestamp).isSameOrAfter(first));
      }
    }
}

const hydrate = create({
  storage: localForage,
  jsonify: false
});

const store = new CreditsStore();

const creditsHydrate = hydrate('credits', store);
creditsHydrate.then(() => {
  // store.cleanUp();
});

export default store;
export { creditsHydrate };
