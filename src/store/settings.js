import { observable, action, computed } from 'mobx';
import { create, persist } from 'mobx-persist';
import axios from 'axios';
import localForage from 'localforage';
import errorStore from 'store/errors';
import moment from 'moment';
import numeral from 'numeral';

import { MOONLIGHT_VERSION } from 'modules/globals';

import 'numeral/locales/fi';
import 'numeral/locales/en-gb';

const mooncloud = axios.create({
  baseURL: 'https://mooncloud.herokuapp.com/api/v1/'
});

class SettingsStore {
    @persist @observable locale = 'en-GB';
    @persist @observable logFolder = '';
    @persist @observable edsmKey = '';
    @persist @observable edsmUser = '';
    @persist @observable enableComments = false;
    @persist @observable enableBookmarks = true;

    @action
    changeLocale(newLocale) {
      moment.locale(newLocale);
      numeral.locale(getNumeralLocale(newLocale));
      this.locale = newLocale;
    }

    @computed
    get isEdsmLoggedIn() {
      return !!this.edsmKey && !!this.edsmUser;
    }

    checkVersion() {
      mooncloud.get('update', {
        params: {
          version: MOONLIGHT_VERSION
        }
      })
        .then(response => {
          if (response.data.status === 'Update needed') {
            errorStore.pushError(`An update is available! Latest version is ${response.data.latestVersion}, you are using ${MOONLIGHT_VERSION}.`);
          }
        }).catch(e => console.error(e));
    }

}


const store = new SettingsStore();


/**
 * Executes the decorated function only if we have EDSM login credentials.
 * @param {*} target 
 * @param {*} name 
 * @param {*} descriptor 
 */
function edsm(target, name, descriptor) {
  if(target.notes) {
    window.sysNotes = target.notes;
  }

  const fn = descriptor.value;

  const edsmCheck = function () {
    if (store.isEdsmLoggedIn) {
      fn.apply(target, arguments);
    }
  };

  descriptor.value = edsmCheck;
  return descriptor;
}


const hydrate = create({
  storage: localForage,
  jsonify: false
});



const settingsHydrate = hydrate('settings', store);
settingsHydrate
  .then(() => {
    moment.locale(store.locale);
    numeral.locale(getNumeralLocale(store.locale));
    store.checkVersion();
    console.log('Settings store hydrated');
  })
  .catch(error => {
    console.error('failed to hydrate settings', error);
    errorStore.pushError('Note saving failed!');
  });

export default store;
export { settingsHydrate, edsm };

const getNumeralLocale = (loc) => {
  switch (loc) {
    case 'fi-FI':
    case 'sv-SE':
      return 'fi';
    default:
      return 'en-gb';
  }
};

window.settings = store;
