import React, { Component } from 'react';
import logStore from 'store/log';
import settingsStore from 'store/settings';
import systemStore from 'store/system';
import errorStore from 'store/errors';
import navStore from 'store/nav';
import creditsStore from 'store/credits';
import { Provider } from 'mobx-react';
import HydrateBuffer from 'components/HydrateBuffer';
import Moonlight from 'components/Moonlight';

export default class StoreWrapper extends Component {

  render() {
    return (
      <HydrateBuffer>
        <Provider
          log={logStore}
          settings={settingsStore}
          system={systemStore}
          errors={errorStore}
          nav={navStore}
          credits={creditsStore}
        >
          <Moonlight />
        </Provider>
      </HydrateBuffer>
    );

  }
}
