import React, { Component } from 'react';
import numeral from 'numeral';

const nf = '0,0.00';

const PlanetDetails = ({ body }) => {
  return (
    <div className="body__details-col">
      {
        body.terraformingState &&
        <div className="body__details-item">
          <span>Terraforming</span><span>{body.terraformingState}</span>
        </div>
      }
      <div className="body__details-item">
        <span>Gravity</span><span>{numeral(body.gravity || 0).format(nf)} G</span>
      </div>
      <div className="body__details-item">
        <span>Earth masses</span><span>{numeral(body.earthMasses || 0).format(nf)}</span>
      </div>
      <div className="body__details-item">
        <span>Radius</span><span>{numeral(body.radius || 0).format(nf)} km</span>
      </div>
      <div className="body__details-item">
        <span>Surface Temperature</span><span>{numeral(body.surfaceTemperature || 0).format()} K</span>
      </div>
      <div className="body__details-item">
        <span>Volcanism</span><span>{body.volcanismType}</span>
      </div>
      <div className="body__details-item">
        <span>Atmosphere</span><span>{body.atmosphereType}</span>
      </div>
      <div className="body__details-item">
        <span>Orbital period</span><span>{numeral(body.orbitalPeriod || 0).format(nf)} d</span>
      </div>
      <div className="body__details-item">
        <span>Rotational period</span><span>{numeral(body.rotationalPeriod || 0).format(nf)} d</span>
      </div>
      <div className="body__details-item">
        <span>Tidally locked</span><span>{body.rotationalPeriodTidallyLocked ? 'yes' : 'no'}</span>
      </div>
    </div>
  );
};

const StarDetails = ({ body }) => {
  return (
    <div className="body__details-col">
      <div className="body__details-item">
        <span>Age</span><span>{numeral(body.age).format()} million years</span>
      </div>
      <div className="body__details-item">
        <span>Solar masses</span><span>{numeral(body.solarMasses).format(nf)}</span>
      </div>
      <div className="body__details-item">
        <span>Solar radius</span><span>{numeral(body.solarRadius).format(nf)}</span>
      </div>
      <div className="body__details-item">
        <span>Surface Temperature</span><span>{numeral(body.surfaceTemperature).format(nf)} K</span>
      </div>
      <div className="body__details-item">
        <span>Orbital period</span><span>{numeral(body.orbitalPeriod || 0).format(nf)} d</span>
      </div>
      <div className="body__details-item">
        <span>Rotational period</span><span>{numeral(body.rotationalPeriod || 0).format(nf)} d</span>
      </div>
      <div className="body__details-item">
        <span>Tidally locked</span><span>{body.rotationalPeriodTidallyLocked ? 'yes' : 'no'}</span>
      </div>
    </div>
  );
};

const StationDetails = ({ station }) => {
  return (
    <div className="body__details-col">
      <div className="body__details-item">
        <span>Allegiance</span><span>{station.allegiance}</span>
      </div>
      <div className="body__details-item">
        <span>Economoy</span><span>{station.economy}</span>
      </div>
      <div className="body__details-item">
        <span>Government</span><span>{station.government}</span>
      </div>
      <div className="body__details-item">
        <span>Controlling faction</span><span>{station.controllingFaction.name}</span>
      </div>
    </div>
  );
};

const StationServices = ({ station }) => {
  return (
    <div className="station__services">
      <div className="station__services-title">Services</div>
      {station.haveMarket && <div className="station__services-service">Market</div>}
      {station.haveShipyard && <div className="station__services-service">Shipyard</div>}
    </div>
  );
};

const Rings = ({ rings, title }) => {
  return (
    <div className="body__rings">
      <div className="body__rings-title">{title}</div>
      <div className="body__rings-ring">
        {
          rings.map(ring => <div key={ring.name} className="body__rings-ring"><span>{ring.name}</span><span>{ring.type}</span></div>)
        }
      </div>
    </div>
  );
};

const Materials = ({ materials }) => {
  return (
    <div className="body__materials">
      <div className="body__materials-title">Materials</div>
      {
        (() => {
          return Object.entries(materials).map(([material, amount]) => {
            return (
              <div className="body__materials-material" key={material}>
                <span>{material}</span><span>{numeral(parseFloat(amount, 10)).format(nf)} %</span>
              </div>
            );
          });
        })()
      }
    </div>
  );
};

const isStation = (body) => {
  return body.type !== 'Planet' && body.type !== 'Star';
};

export default class BodyView extends Component {

  render() {
    const {
      body
    } = this.props;

    if (!body) {
      return <div className="body__wrap"></div>;
    }

    const isStar = body.type === 'Star';
    const isPlanet = body.type === 'Planet';

    return (
      <div className="body__wrap">
        <div className="body__main-bar">
          <div>
            <div className="body__title">
              {body.name}
            </div>
            <div className="body__type">
              {isStation(body) ? body.type : body.subType}
            </div>
          </div>
          <div className="body__landable">
            {body.isLandable ? 'Landable' : body.isScoopable ? 'Scoopable' : ''}
          </div>
        </div>
        <div className="body__details">
          {
            isPlanet ? <PlanetDetails body={body} /> :
              isStar ? <StarDetails body={body} /> :
                <StationDetails station={body} />
          }
          <div className="body__details-col">
            {(body.rings || body.belts) && <Rings rings={body.rings || body.belts} title={body.rings ? 'Rings' : 'Belts'} />}
            {body.materials && <Materials materials={body.materials} />}
            {isStation(body) && <StationServices station={body} />}
          </div>
        </div>
      </div>
    );
  }
}

/*



*/
