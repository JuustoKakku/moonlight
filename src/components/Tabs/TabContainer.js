import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Tab from './Tab';
import classNames from 'classnames';

class TabError extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      err: false
    };
  }

  componentDidCatch(error, info) {
    console.log(error, info);
    this.setState({
      err: true
    });
  }

  render() {
    const {
      err
    } = this.state;

    if (err) {
      return <div className="tabs__error">Something went wrong and the tab failed to load!</div>;
    }
    return this.props.children;
  }
}

class TabContainer extends Component {

  constructor(props) {
    super(props);

    this.state = {
      tabs: [],
      activeTab: ''
    };
  }

  showChild(child) {
    if(child.props.isVisible && typeof child.props.isVisible === 'function') {
      return child.props.isVisible();
    }

    return true;
  }

  checkChildren(props) {
    const newTabs = [];
    let newActive = this.state.activeTab;

    // With only one child, that child is directly in the children props,
    // with more, it's an array
    if (Array.isArray(props.children)) {
      for (const child of props.children) {
        if (child.type === Tab && this.showChild(child)) {
          newTabs.push({
            name: child.props.name,
            childComponent: child.props.component
          });
          if (!newActive) {
            newActive = child.props.name;
          }
        }
      }
    } else {
      if (props.children.type === Tab && this.showChild(props.children)) {
        newTabs.push({
          name: props.children.props.name,
          childComponent: props.children.props.component
        });
        if (!newActive) {
          newActive = props.children.props.name;
        }
      }
    }
    this.setState({
      tabs: newTabs,
      activeTab: newActive
    }, () => {
      // If the tabs change, make sure one of the visible ones is active
      if(this.state.tabs.filter(tab => tab.name === this.state.activeTab).length === 0 && this.state.tabs.length > 0) {
        this.setState({
          activeTab: this.state.tabs[0].name
        });
      }
    });
  }

  componentDidMount() {
    this.checkChildren(this.props);
  }

  componentWillReceiveProps(nextProps) {
    this.checkChildren(nextProps);
  }

    selectTab = (newTab) => {
      this.setState({
        activeTab: newTab
      }, () => {
        if (this.props.onChange) {
          this.props.onChange(newTab);
        }
      });
    }

    render() {
      const {
        tabs,
        activeTab
      } = this.state;
      return (
        <div className="tabs__container">
          {tabs.length > 1 ? <div className="tabs__row">
            {tabs.map(tab => {
              const tabCls = classNames('tabs__btn', { 'tabs__active': activeTab === tab.name });
              return <div className={tabCls} key={tab.name} onClick={() => this.selectTab(tab.name)}>{tab.name}</div>;
            })}
          </div> : null}
          <div className="tabs__component">
            <TabError>
              {(() => {
                if (!tabs || tabs.length === 0) {
                  return null;
                }
                const activeChild = tabs.filter(tab => tab.name === activeTab);
                if (activeChild.length > 0) {
                  return activeChild[0].childComponent;
                } else {
                  return null;
                }
              })()}
            </TabError>
          </div>
        </div>
      );
    }
}

TabContainer.propTypes = {
  onChange: PropTypes.func
};

export default TabContainer;
