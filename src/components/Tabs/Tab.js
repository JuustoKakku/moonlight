import { Component } from 'react';
import PropTypes from 'prop-types';

class Tab extends Component {

  // This is not actually used...
  render() {
    return this.props.component;
  }
}

Tab.propTypes = {
  name: PropTypes.string.isRequired,
  component: PropTypes.node.isRequired,
  isVisible: PropTypes.func
};


export default Tab;
