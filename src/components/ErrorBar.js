import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

@inject('errors')
@observer
export default class ErrorBar extends Component {

  render() {
    const {
      latestError
    } = this.props.errors;
        
    const {
      close,
      transitionEnd
    } = this.props;

    return (
      <div className="error__container" onTransitionEnd={transitionEnd}>
        {latestError}
        <span onClick={close} className="error__close"><i className="fa fa-close"></i></span>
      </div>
    );
  }
}
