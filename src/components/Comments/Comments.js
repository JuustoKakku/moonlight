import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';

@inject('system', 'settings')
@observer
export default class Comments extends Component {

  render() {
    const {
      systemName
    } = this.props;

    const {
      currentComment
    } = this.props.system;

    const {
      isEdsmLoggedIn
    } = this.props.settings;

    return (
      <div className="comments__container">
        <span className="comments__title">
                    Notes for {systemName}
        </span>
        <span className="comments__desc text">
                    The system notes are saved privately on your EDSM.net account.
        </span>
        <textarea
          value={currentComment}
          ref={el => this.commentInput = el}
          onChange={() => this.props.system.setComment(systemName, this.commentInput.value)} />
        <div className="comments__buttons">
          {!isEdsmLoggedIn ? <span className="text comments__disabled">To save notes, add your EDSM credentials in settings.</span> : null}
          <button onClick={() => this.props.system.updateSystemComment(systemName)} disabled={!isEdsmLoggedIn}>Save</button>
        </div>
      </div>
    );
  }
}
