import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import ScrollArea from 'components/ScrollArea';
import { MOONLIGHT_NAME, MOONLIGHT_VERSION } from 'modules/globals';


// moment.locales() seems to only return those loaded so far
// so list some here...
const locales = [
  'en-US', 'en-GB',
  'fi-FI', 'sv-SE'
];


@inject('settings')
@observer
export default class Settings extends Component {

  constructor(props) {
    super(props);
    this.state = {
      comments: props.settings.enableComments,
      bookmarks: props.settings.enableBookmarks
    };
  }

    clickBlock = (e) => {
      e.preventDefault();
      e.stopPropagation();
    }

    acceptHandler = () => {
      const {
        setSettings,
        settings
      } = this.props;

      const {
        bookmarks,
        comments
      } = this.state;

      settings.changeLocale(this.locale.value);
      settings.edsmUser = (this.edsmUser.value || '').trim();
      settings.edsmKey = (this.edsmKey.value || '').trim();
      settings.enableComments = comments;
      settings.enableBookmarks = bookmarks;

      setSettings(false);
    }

    onCheckboxChange = (box) => {
      this.setState({
        [box]: !this.state[box]
      });
    }

    render() {
      const {
        visible,
        settings,
        setSettings
      } = this.props;

      if (!visible) {
        return null;
      }

      const {
        comments,
        bookmarks
      } = this.state;


      return (
        <div className="settings__background" onClick={this.clickBlock}>
          <div className="settings__window">
            <div className="settings__form-wrap">
              <ScrollArea>
                <div className="settings__form">
                  <div className="settings__item">
                    <span className="settings__label">Locale</span>
                    <select ref={el => this.locale = el} defaultValue={settings.locale}>
                      {locales.map(locale => <option key={locale} value={locale}>{locale}</option>)}
                    </select>
                  </div>
                  <div className="settings__item">
                    <span className="settings__label">EDSM User name</span>
                    <input type="text" ref={el => this.edsmUser = el} defaultValue={settings.edsmUser} />
                  </div>
                  <div className="settings__item">
                    <span className="settings__label">EDSM API key</span>
                    <input type="text" ref={el => this.edsmKey = el} defaultValue={settings.edsmKey} />
                  </div>
                  <div className="settings__item">
                    <span className="settings__label">Enable notes</span>
                    <input type="checkbox" className="checkbox" id="note-tick" checked={comments} onChange={() => {}} />
                    <label className="checkbox" htmlFor="note-tick" onClick={() => this.onCheckboxChange('comments')}></label>
                  </div>
                  <div className="settings__item">
                    <span className="settings__label">Enable bookmarks</span>
                    <input type="checkbox" className="checkbox" id="bookmark-tick" checked={bookmarks} onChange={() => {}} />
                    <label className="checkbox" htmlFor="bookmark-tick" onClick={() => this.onCheckboxChange('bookmarks')}></label>
                  </div>
                </div>
              </ScrollArea>
              <div className="settings__version">
                {MOONLIGHT_NAME} {MOONLIGHT_VERSION}
              </div>
            </div>
            <div className="settings__buttons">
              <button onClick={this.acceptHandler}>Accept</button>
              <button onClick={() => setSettings(false)}>Cancel</button>
            </div>
          </div>
        </div>
      );
    }


}
