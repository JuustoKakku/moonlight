import 'assets/css/Moonlight.css';
import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import SystemLog from 'components/SystemLog';
import ExplorationView from 'components/ExplorationView';
import TopBar from 'components/TopBar';
import Settings from 'components/Settings';
import ErrorBar from 'components/ErrorBar';
import classNames from 'classnames';
import { tabs } from 'store/nav';
import CreditsView from 'components/credits/CreditsView';
import { remote } from 'electron';

@inject('errors', 'nav')
@observer
class Moonlight extends Component {

  constructor(props) {
    super(props);

    this.state = {
      settingsOpen: false,
      showError: false,
      errorClosing: false,
      maximized: false
    };
  }

  componentDidMount() {
    const win = remote.getCurrentWindow();
    win.on('maximize', () => {
      this.setState({
        maximized: true
      });
    });
    win.on('unmaximize', () => {
      this.setState({
        maximized: false
      });
    });
  }

    setSettingsState = (open) => {
      if (open === undefined) {
        this.setState({
          settingsOpen: !this.state.settingsOpen
        });
      } else {
        this.setState({
          settingsOpen: open
        });
      }
    }

    componentWillUpdate(nextProps, nextState) {
      if (nextProps.errors.hasError && !nextState.showError && !nextState.errorClosing) {
        this.setState({
          showError: true
        });
      }

    }

    hideError = () => {
      this.setState({
        showError: false,
        errorClosing: true
      });
    }

    errorTransitionHandler = () => {
      if (!this.state.showError) {
        this.props.errors.removeLatestError();
        this.setState({
          errorClosing: false
        });
      }
    }

    render() {
      const {
        settingsOpen,
        showError,
        maximized
      } = this.state;

      const {
            hasError // eslint-disable-line
      } = this.props.errors;

      const {
        currentTab
      } = this.props.nav;

      const mainCls = classNames('main-wrap', { 'main-wrap__settings-open': settingsOpen, 'main-wrap__has-error': showError, 'main-wrap__maximized': maximized });

      return (
        <div className={mainCls}>
          <TopBar
            setSettings={this.setSettingsState}
          />
          <ErrorBar close={this.hideError} transitionEnd={this.errorTransitionHandler} />
          <div className="main-wrap__container">
            {
              (() => {
                switch (currentTab) {
                  case tabs.TAB_EXPLORATION:
                    return (
                      <div className="main-wrap__content">
                        <SystemLog />
                        <ExplorationView />
                      </div>
                    );
                  case tabs.TAB_CREDITS:
                    return (
                      <div className="main-wrap__content">
                        <CreditsView />
                      </div>
                    );
                  default:
                    return (
                      <div className="main-wrap__content">

                      </div>
                    );

                }
              })()
            }
            <Settings visible={settingsOpen} setSettings={this.setSettingsState} />
          </div>
        </div>
      );


    }
}

export default Moonlight;
