import React, { Component } from 'react';
import { settingsHydrate } from 'store/settings';
import { logHydrate } from 'store/log';
import { creditsHydrate } from 'store/credits';
import { navHydrate } from 'store/nav';
import Splash from 'components/Splash';

export default class HydrateBuffer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      hydrated: false
    };
  }

  componentDidMount() {
    Promise.all([
      settingsHydrate,
      logHydrate,
      creditsHydrate,
      navHydrate
    ]).then(() => {
      this.setState({
        hydrated: true
      });
    }).catch((err) => console.error(err));
  }

  render() {
    const {
      hydrated
    } = this.state;

    if(!hydrated) {
      return <Splash />;
    }

    return this.props.children;
  }

}
