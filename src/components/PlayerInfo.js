import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import numeral from 'numeral';
import classNames from 'classnames';
import { tabs } from 'store/nav';

@inject('log', 'settings', 'nav')
@observer
export default class PlayerInfo extends Component {

    setTab = (tab) => {
      this.props.nav.currentTab = tab;
    }

    render() {
      const {
        cmdr,
        shipType,
        shipName,
        shipIdent,
        credits,
        fuelLevel,
        fuelCapacity
      } = this.props.log;

      const {
        currentTab
      } = this.props.nav;

        // Need to listen to changes to locale,
        // as it is used indirectly through numeral.
        // Otherwise, numbers stay in the wrong format until
        // some other update.
      const {
            locale // eslint-disable-line
      } = this.props.settings;

      const fuelPct = Math.round((fuelLevel / fuelCapacity) * 100);
      const fuelStyle = {
        width: `${fuelPct}%`
      };

      const cmdrCls = classNames('player-info__tab', { 'player-info__active': currentTab === tabs.TAB_CMDR });
      const fleetCls = classNames('player-info__tab', { 'player-info__active': currentTab === tabs.TAB_FLEET });
      const creditsCls = classNames('player-info__tab', { 'player-info__active': currentTab === tabs.TAB_CREDITS });
      const expCls = classNames('player-info__tab player-info__fuel', { 'player-info__active': currentTab === tabs.TAB_EXPLORATION });
        
      return (
        <div className="player-info__container">
          <span className={cmdrCls} >
            <span><span className="player-info__label">CMDR</span> {cmdr}</span>
          </span>
          <span className={fleetCls}>
            <span><span className="player-info__label">Ship</span> { shipName ? `${shipName} [${shipIdent}]` : shipType }</span>
          </span>
          <span className={creditsCls} onClick={() => this.setTab(tabs.TAB_CREDITS)}>
            <span><span className="player-info__label">Credits</span> {numeral(credits).format()}</span>
          </span>
          <div className={expCls} onClick={() => this.setTab(tabs.TAB_EXPLORATION)}>
            <span>
              <span className="player-info__label">Fuel</span>
              <span className="player-info__fuel-bg">
                <span className="player-info__fuel-bar" style={fuelStyle}></span>
              </span>
              <span>{`${fuelPct}%`}</span>
            </span>
          </div>
        </div>
      );

    }
}
