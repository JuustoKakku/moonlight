import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import moment from 'moment';
import classnames from 'classnames';
import ScrollArea from './ScrollArea';
import TabContainer, { Tab } from 'components/Tabs';

const ECO_NONE = '$economy_None;';

const LogItem = ({ item, selected, select, logIndex, addBookmark, isBookmarked }) => {
  const itemClass = classnames('log-item', { 'log-item__selected': selected });

  return (
    <div className={itemClass} onClick={() => select(logIndex)} onContextMenu={() => addBookmark(item.StarSystem)}>
      <span className="log-item__title-row">
        <span className="log-item__title">{isBookmarked ? <i className="fa fa-bookmark"></i> : null}{item.StarSystem}</span>
        <span>{item.SystemEconomy !== ECO_NONE ? item.SystemEconomy_Localised : ''}</span>
      </span>
      <div className="log-item__info-row">
        <span>{moment(item.timestamp).format('L')}&nbsp;{moment(item.timestamp).format('LT')}</span>
        <span>{item.SystemAllegiance}</span>
      </div>
    </div>
  );
};

@inject('log', 'settings')
@observer
export default class SystemLog extends Component {

    onTabChange = (tabName) => {
      if (tabName === 'Log') {
        const {
          selectedEntry,
          selectEntry
        } = this.props.log;
        selectEntry(selectedEntry);
      } else {
        const {
          selectedBookmark,
          selectBookmark
        } = this.props.log;
        selectBookmark(selectedBookmark);
      }
    }

    render() {
      const {
        jumpLog,
        selectedEntry,
        selectEntry,
        bookmarks,
        addBookmark,
        selectBookmark,
        selectedBookmark,
        isBookmarked
      } = this.props.log;

      const {
        locale,
        enableBookmarks
      } = this.props.settings;


      return (
        <div className="log-container">
          <TabContainer onChange={this.onTabChange}>
            <Tab
              name="Log"
              component={
                <ScrollArea>
                  <div className="log-wrap">
                    {jumpLog.map((item, index) => <LogItem
                      key={item.timestamp}
                      item={item}
                      selected={index === selectedEntry}
                      select={selectEntry}
                      logIndex={index}
                      locale={locale}
                      addBookmark={enableBookmarks ? addBookmark : () => {}}
                      isBookmarked={enableBookmarks && isBookmarked(item.StarSystem)}
                    />
                    )}
                  </div>
                </ScrollArea>
              }
            />
            <Tab
              name="Bookmarks"
              isVisible={() => enableBookmarks}
              component={
                (() => {
                  if (bookmarks.length === 0) {
                    return (
                      <div className="log__empty">
                                            You have no bookmarks!<br />
                                            Right click on log items to bookmark them.
                      </div>
                    );
                  } else {
                    return (
                      <ScrollArea>
                        <div className="log-wrap">
                          {bookmarks.map((item, index) => <LogItem
                            key={item.timestamp}
                            item={item}
                            selected={index === selectedBookmark}
                            select={selectBookmark}
                            addBookmark={addBookmark}
                            logIndex={index}
                            locale={locale}
                            isBookmarked={isBookmarked(item.StarSystem)}
                          />
                          )}
                        </div>
                      </ScrollArea>
                    );
                  }
                })()
              }
            />
          </TabContainer>
        </div>
      );
    }
}
