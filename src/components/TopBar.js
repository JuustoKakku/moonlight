import React, { Component } from 'react';
import PlayerInfo from 'components/PlayerInfo';
import { remote } from 'electron';

export default class TopBar extends Component {

    onCloseClick = () => {
      remote.getCurrentWindow().close();
    }

    render() {
      const {
        setSettings
      } = this.props;

      return (
        <div className="top-bar">
          <div className="top-bar__settings" onClick={() => setSettings()}>
            <i className="fa fa-bars"></i>
          </div>
          <PlayerInfo />
          <div className="top-bar__close" onClick={this.onCloseClick}>
            <i className="fa fa-times"></i>
          </div>
        </div>
      );
    }
}
