import React, { Component } from 'react';
import { Scrollbars } from 'react-custom-scrollbars';

class ScrollArea extends Component {
  render() {
    return (
      <Scrollbars
        renderTrackHorizontal={props => <div {...props} className="scroll-track-hor" />}
        renderTrackVertical={props => <div {...props} className="scroll-track-ver" />}
        renderThumbHorizontal={props => <div {...props} className="scroll-btn-hor" />}
        renderThumbVertical={props => <div {...props} className="scroll-btn-ver" />}
        renderView={props => <div {...props} className="scroll-view" />}>
        {this.props.children}
      </Scrollbars>
    );
  }
}

export default ScrollArea;
