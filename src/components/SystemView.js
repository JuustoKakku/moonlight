import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { intercept } from 'mobx';
import ScrollArea from 'components/ScrollArea';
import classNames from 'classnames';
import numeral from 'numeral';
import BodyView from 'components/BodyView';
import Comments from 'components/Comments';

const BodyItem = ({ body, select, selected }) => {
  const bodyCls = classNames(`system__body system__body-offset-${body.offset}`, { 'system__body-selected': selected && body && selected.name === body.name });

  let bodyIcon = null;
  switch (body.type) {
    case 'Star':
      bodyIcon = <i className="icon-star system__body-icon"></i>;
      break;
    case 'Planet':
      bodyIcon = <i className="icon-planet system__body-icon"></i>;
      break;
    case 'Ocellus Starport':
      bodyIcon = <i className="icon-ocellus system__body-icon"></i>;
      break;
    case 'Orbis Starport':
      bodyIcon = <i className="icon-orbis system__body-icon"></i>;
      break;
    case 'Coriolis Starport':
      bodyIcon = <i className="icon-coriolis system__body-icon"></i>;
      break;
    case 'Planetary settlement': // actually a surface port
      bodyIcon = <i className="icon-surface-port system__body-icon"></i>;
      break;
    case 'Outpost':
      bodyIcon = <i className="icon-outpost system__body-icon"></i>;
      break;
    case 'Asteroid base':
      bodyIcon = <i className="icon-asteroid-base system__body-icon"></i>;
      break;
    default:
      console.log(body);
      break;
  }

  return (
    <div className={bodyCls} onClick={() => select(body)}>
      <span className="system__body-title">
        {bodyIcon}{body.name}{body.isLandable ? <span>(L)</span> : body.isScoopable ? <span>(S)</span> : null}
      </span>
      <span className="system__body-distance number">
        {numeral(body.distanceToArrival).format()} Ls
      </span>
    </div>
  );
};

@inject('system', 'settings')
@observer
export default class SystemView extends Component {

  constructor(props) {
    super(props);

    this.state = {
      selectedBody: null
    };
  }

  componentDidMount() {
    this.interceptor = intercept(this.props.system, 'selectedSystemName', (change) => {
      if (this.props.system.selectedSystemName !== change.newValue) {
        this.clearSelection();
      }
      return change;
    });
  }

  componentWillUnmount() {
    this.interceptor();
  }

    clearSelection = () => {
      this.setState({
        selectedBody: null
      });
    }

    selectBody = (body) => {
      this.setState({
        selectedBody: body
      });
    }

    render() {
      const {
        selectedSystem,
        selectedSystemName
      } = this.props.system;

      const {
        selectedBody
      } = this.state;

      const {
        showComments
      } = this.props;

      const {
        enableComments
      } = this.props.settings;

      const loading = this.props.system.isSystemLoading(selectedSystemName);

      if (loading && (!selectedSystem || selectedSystem.bodies.length === 0) ) {
        return (
          <div className="system__wrap">
            <div className="system__body-list--container">
              <span className="system__title">Scanning...</span>
            </div>
          </div>
        );
      }

      if (!selectedSystem) {
        return (
          <div className="system__wrap">
            <div className="system__body-list--container">
              <span className="system__title">No data</span>
            </div>
          </div>
        );
      }

      return (
        <div className="system__wrap">
          <div className="system__body-list--container">
            {
              (() => {
                if (selectedSystem.bodies.length === 0) {
                  return <span className="system__empty">No known bodies</span>;
                } else {
                  return (
                    <div className="system__body-list--wrap">
                      <ScrollArea>
                        <div className="system__body-list">
                          {selectedSystem.bodies.map(body => <BodyItem body={body} key={body.id} select={this.selectBody} selected={selectedBody} locale={this.props.settings.locale} />)}
                        </div>
                      </ScrollArea>
                    </div>
                  );
                }
              })()
            }
          </div>
          {
            (() => {
              if (selectedBody || (showComments && enableComments) ) {
                return (
                  <div className="system__body-info--container">
                    <BodyView body={selectedBody} />
                    { enableComments && showComments ? <div className="system__body-info--comments"><Comments systemName={selectedSystemName} /></div> : null}
                  </div>
                );
              }
            })()
          }
        </div>
      );
    }
}
