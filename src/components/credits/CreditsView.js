import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import moment from 'moment';
import numeral from 'numeral';

const colors = {
  axis: '#5876BF',
  background: '#28395D',
  graph: '#B3E0FF'
};


@inject('credits', 'settings')
@observer
export default class CreditsView extends Component {

  componentDidMount() {
    this.ctx = this.canvas.getContext('2d');

    this.updateGraph();

    window.addEventListener('resize', this.updateGraph);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateGraph);
  }

  checkSize() {
    const style = getComputedStyle(this.sizer);
    const w = style.width;
    const h = style.height;

    this.canvas.width = w.replace('px' ,'');
    this.canvas.height = h.replace('px', '');
  }

  clearCanvas() {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }

  drawAxis(width, height) {       
    const c = this.ctx;

    c.globalAlpha = 0.13;
    c.fillStyle = colors.background;
    c.rect(0, 0 - height, width, height);
    c.fill();

    c.beginPath();
    c.globalAlpha = 1;
    c.strokeStyle = colors.axis;
    c.lineWidth = 3;
    c.moveTo(0, 0 - height);
    c.lineTo(0, 0);
    c.lineTo(0 + width, 0);
    c.stroke();
  }

  drawCreditLine(width, height) {
    const start = this.props.credits.firstDate;
    const end = this.props.credits.lastDate;
    const max = this.props.credits.maxCredits;

    const c = this.ctx;

    c.beginPath();
    c.lineWidth = 4;
    c.strokeStyle = colors.graph;
    let move = true;

    this.props.credits.history.forEach((entry) => {
      const x = ((moment(entry.timestamp).unix() - start) / (end - start)) * width;
      const y = ((entry.credits / max) * height) * (-1);

      if(move) {
        c.moveTo(x, y);
        move = false;
      } else {
        c.lineTo(x, y);
      }
    });

    c.stroke();
  }

  drawGrid(width, height) {
    const c = this.ctx;
    const max = this.props.credits.maxCredits;
    const start = this.props.credits.firstDate;
    const end = this.props.credits.lastDate;
    const dateRange = end - start;
    
    c.lineWidth = 1;
    c.strokeStyle = colors.axis;
    c.fillStyle = colors.axis;
        
    c.font = '12px Telegrama';
    c.textAlign = 'right';
    c.fillText('0', 0 - 7, 0);
    c.textAlign = 'left';
    c.save();
    c.rotate(Math.PI * -1.7);
    c.fillText(moment.unix(start).format('L'), 15, 0);
    c.restore();
        
    const pct = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0];
    pct.forEach(p => {
      // Horizontal credit lines
      c.globalAlpha = 0.4;
      const yPos = p * height * -1;
      c.beginPath();
      c.moveTo(0, yPos);
      c.lineTo(width, yPos);
      c.stroke();
            
      // Credit texts
      c.globalAlpha = 1;
      c.textAlign = 'right';
      c.fillText(numeral(max * p).format(), 0 - 7, yPos + 7);
            
      // Vertical date lines
      c.globalAlpha = 0.4;
      c.textAlign = 'left';
      const xPos = p * width;
      c.beginPath();
      c.moveTo(xPos, 0);
      c.lineTo(xPos, height * -1);
      c.stroke();
            
      // Date texts
      c.globalAlpha = 1;
      c.save();
      c.translate(xPos, 0);
      c.rotate(Math.PI * -1.7);
      c.fillText(moment.unix((dateRange * p) + start).format('L'), 15, 0);
      c.restore();
    });
  }



    updateGraph = () => {
      this.checkSize();

      const cv = this.canvas;
      const c = this.ctx;

      const left = 100;
      const right = 5;
      const top = 5;
      const bottom = 150;

      const ox = left;
      const oy = cv.height - bottom;
        
      const gridWidth = cv.width - left - right;
      const gridHeight = cv.height - top - bottom;

      this.clearCanvas();

      c.translate(ox, oy);
        
      this.drawAxis(gridWidth, gridHeight);
      this.drawGrid(gridWidth, gridHeight);
      this.drawCreditLine(gridWidth, gridHeight);
    }

    componentWillReceiveProps() {
      this.updateGraph();
    }

    render() {

      const {
            locale // eslint-disable-line
      } = this.props.settings;

      return (
        <div className="credits__container">
          <div className="credits__sizer" ref={el => this.sizer = el}>
            <canvas ref={el => this.canvas = el}></canvas>
          </div>
        </div>  
      );
    }
}

