import React, { Component } from 'react';
import SystemView from 'components/SystemView';
import TabContainer, { Tab } from 'components/Tabs';
import { WINDOW_SIZE_LARGE } from 'modules/globals';
import Comments from 'components/Comments';
import { inject, observer } from 'mobx-react';

@inject('system', 'settings')
@observer
export default class ExplorationView extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: window.innerWidth
    };
  }

    resizeListener = () => {
      this.setState({
        width: window.innerWidth
      });
    }

    componentDidMount() {
      window.addEventListener('resize', this.resizeListener);
    }

    componentWillUnmount() {
      window.removeEventListener('resize', this.resizeListener);
    }

    showCommentTab = () => {
      return this.state.width <= WINDOW_SIZE_LARGE;
    }

    render() {
      const {
        selectedSystemName
      } = this.props.system;
      const {
        enableComments
      } = this.props.settings;
      return(
        <TabContainer>
          <Tab
            name="Bodies"
            component={
              <div className="main-wrap__content--system-wrap">
                <SystemView showComments={!this.showCommentTab()} />
              </div>
            }
          />
          <Tab
            name="Notes"
            component={<Comments systemName={selectedSystemName} />}
            isVisible={() => enableComments && this.showCommentTab()}
          />
        </TabContainer>
      );
    }
}
